Otimização de Portfólio usando Computação Quântica: Benchmark Ket vs. Qiskit vs. myQLM
======================================================================================

Benchmark adaptado do Jupyter Notebook que acompanha o artigo `Quantum Finance: um tutorial de computação quântica aplicada ao mercado
financeiro` disponível em https://www.dualq.tech/tutoriais/quantum-finance.

O Jupyter Notebook, originalmente implementado usando myQLM, está disponível em `askery/computacao-quantica-aplicada-ao-mercado-financeiro <https://github.com/askery/computacao-quantica-aplicada-ao-mercado-financeiro>`_.


Resultado
---------

.. csv-table:: Tempo de Execução
   :header-rows: 1

   Ket (KBW), Qiskit (qasm; 16384 shots), Qiskit (statevector),  myQLM (PyLinalg)
   0:20:39, 1:17:35, 2:56:7, 1:28:54


Usando os simuladores KBW, statevector e PyLinalg conseguimos retornar a probabilidade exata de medir um determinado estado da base computacional.
Isso não é possível com o simulador qasm. Nesse casso, usamos 16384 shots/execuções para aproximar a probabilidade.

.. csv-table:: Tempo Relativo de Execução
   :header-rows: 1
   :stub-columns: 1

     ↓÷→ , Ket, Qiskit (qasm), Qiskit (statevector),  myQLM
     Ket, 100.00%, 26.62%, 11.73%, 23.23%
     Qiskit (qasm), 375.67%, 100.00%, 44.06%, 87.28%
     Qiskit (statevector), 852.71%, 226.99%, 100.00%, 198.11%
     myQLM, 430.42%, 114.57%, 50.48%, 100.00%

| Porcentagem do tempo de execução da plataforma da coluna Y que a plataforma da linha X usou para terminar a execução.
| Se o valor na linha Y, coluna X for menor que 100%, a plataforma Y executou mais rápido que a plataforma X.
| Exemplo, o myQLM usou 50.48% do tempo que o Qiskit (statevector) levou para terminar a execução, logo myQLM foi mais rápido.


Configurações Usadas no Benchmark
---------------------------------

* Imagem Docker: ``jupyter/datascience-notebook:python-3.9.12``
* Pacotes Python:
   - ket-lang == 0.4.0.dev2
   - qiskit == 0.36.2
   - myqlm == 1.5.1
   - pyqubo == 1.2.0
* Linux: 5.17.11-300.fc36.x86_64
* Docker: 20.10.16, build aa7e414
* CPU: Intel Core i7-8565U
